package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.Static("/css", "./views/css")
	r.Static("/js", "./views/js")
	r.Static("/sass", "./views/sass")
	r.Static("/fonts", "./views/fonts")
	r.Static("/img", "./views/img")
	r.LoadHTMLGlob("views/*.html")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})
	log.Fatal(r.Run())
}
